#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import json
import os
import random

from hermes_python.hermes import Hermes
from hermes_python.ontology import *

file_path = os.path.dirname(os.path.realpath(__file__))


def read_riddles():
    """ Read all riddles from file """

    file = open(file_path + "/riddles.json")
    riddles = json.load(file)
    file.close()
    return riddles


def check_current_riddle():
    """ Get current riddle id from status file """

    name = file_path + "/status.txt"
    file = open(name, "r")
    line = next(file)
    file.close()
    return int(line)


def write_current_riddle(riddle_id):
    """ Write riddle id in status file """

    name = file_path + "/status.txt"
    file = open(name, "w")
    file.write(str(riddle_id))
    file.close()


def callback_get_riddle(hermes, intent_message):
    """ Read new riddle or repeat open riddle """

    riddle_id = check_current_riddle()
    riddles = read_riddles()

    if (riddle_id == None or riddle_id == 0):
        riddle = random.choice(riddles)
        write_current_riddle(riddle["id"])
        result_sentence = riddle["question"]
    else:
        riddle = [r for r in riddles if r["id"] == riddle_id][0]
        result_sentence = riddle["question"]

    current_session_id = intent_message.session_id
    hermes.publish_end_session(current_session_id, result_sentence)


def callback_check_riddle(hermes, intent_message):
    """ Check if the solution of the user is correct """

    riddle_id = check_current_riddle()
    riddles = read_riddles()

    if (riddle_id == None or riddle_id == 0):
        result_sentence = "Du hast gerade kein offenes Rätsel. Frage nach einem Neuen."
    else:
        if (intent_message.slots is not None and len(intent_message.slots.solution) >= 1):
            riddle = [r for r in riddles if r["id"] == riddle_id][0]
            solution = str(intent_message.slots.solution.first().value)

            if (solution.lower() == riddle["answer"].lower()):
                write_current_riddle(0)
                result_sentence = "Super, du hast das Rätsel gelöst."
            else:
                result_sentence = solution + " ist leider falsch. Versuch was anderes."
        else:
            result_sentence = "Ich habe leider das Lösungswort nicht verstanden."

    current_session_id = intent_message.session_id
    hermes.publish_end_session(current_session_id, result_sentence)


if __name__ == "__main__":
    with Hermes("localhost:1883") as h:
        h.subscribe_intent("CodeSpoof:getRiddle", callback_get_riddle)
        h.subscribe_intent("CodeSpoof:checkRiddle", callback_check_riddle)
        h.start()
