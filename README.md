# Snips-Riddles
Alle Tage geh ich aus, bleibe dennoch stets zu Haus. Wer bin ich?

Dieses Projekt ist ein Fork von DANBERS Riddles Projekt:
https://gitlab.com/DANBER/Snips-Riddles

### Quellen
- https://www.thoughtco.com/german-riddles-1444309
- https://www.germanlw.com/riddles/
- https://www.raetselstunde.de/text-raetsel/vers-raetsel/vers-raetsel-001.html
- https://www.janko.at
